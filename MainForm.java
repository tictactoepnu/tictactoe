package com.Tictactoe;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class MainForm extends JFrame {
    JPanel panel = new JPanel ();
    int size = 3;
    XOButton buttons[] = new XOButton[size*size];

    public MainForm(){
        setTitle("Tictactoe");
        setBounds(300,300,500,500);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        panel.setLayout(new GridLayout(size,size));
        for(int i = 0;i<size*size;i++) {
        buttons[i] = new XOButton();
        panel.add(buttons[i]);
        }
        add(panel);
        setVisible(true);

    }
}
