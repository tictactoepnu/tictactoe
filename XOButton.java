package com.Tictactoe;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class XOButton extends JButton implements ActionListener {

    ImageIcon X,O;
   static boolean checkHit = false;

    public XOButton(){

        X=new ImageIcon(this.getClass().getResource("X.png"));
        O=new ImageIcon(this.getClass().getResource("O.png"));
        this.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e) {
        if(!checkHit) {
            setIcon(O);
            setBackground(new Color(220, 20, 60));
        }
        else {
            setIcon(X);
            setBackground(new Color(32, 178, 170));
        }
        setEnabled(false);
        checkHit = !checkHit;
    }
}
